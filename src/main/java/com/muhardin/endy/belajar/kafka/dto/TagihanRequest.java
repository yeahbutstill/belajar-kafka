package com.muhardin.endy.belajar.kafka.dto;

import java.math.BigDecimal;

import lombok.Data;

@Data
public class TagihanRequest {
    private String debitur;
    private String jenisTagihan;
    private BigDecimal nilai;
}
