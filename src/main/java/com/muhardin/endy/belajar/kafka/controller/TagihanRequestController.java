package com.muhardin.endy.belajar.kafka.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.muhardin.endy.belajar.kafka.dto.TagihanRequest;

import lombok.extern.slf4j.Slf4j;

@RestController @Slf4j
public class TagihanRequestController {

    @Autowired private KafkaTemplate<String, String> kafkaTemplate;
    @Autowired private ObjectMapper objectMapper;

    @Value("${kafka.topic.tagihan-request}")
    private String topicTagihanRequest;

    @PostMapping("/api/tagihan/")
    public void kirimTagihanRequest(@RequestBody TagihanRequest request) throws JsonProcessingException {
        String msg = objectMapper.writeValueAsString(request);
        log.info("Mengirim message {}", msg);
        kafkaTemplate.send(topicTagihanRequest, msg);
    }
}
